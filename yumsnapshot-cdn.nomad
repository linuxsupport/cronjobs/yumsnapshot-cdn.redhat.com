job "${PREFIX}_yumsnapshot-cdn" {
  datacenters = ["*"]

  type = "batch"

  periodic {
    cron = "${SCHEDULE}"
    time_zone = "Europe/Zurich"
    prohibit_overlap = true
  }

  task "${PREFIX}_yumsnapshot-cdn" {
    driver = "docker"

    config {
      image = "https://gitlab-registry.cern.ch/linuxsupport/cronjobs/yumsnapshot-cdn.redhat.com/yumsnapshot-cdn:${CI_COMMIT_SHORT_SHA}"
      logging {
        config {
          tag = "${PREFIX}_yumsnapshot-cdn"
        }
      }
      volumes = [
        "/mnt/data1:/mnt/data1",
      ]
    }

    env {
      YUMSNAPSHOT_DESTINATION = "$YUMSNAPSHOT_DESTINATION"
      TAG = "${PREFIX}_yumsnapshot-cdn"
    }

    resources {
      cpu = 100 # Mhz
      memory = 128 # MB
    }

  }
}
