#!/bin/bash
#
# lxoft-sync-yumsnapshot-cdn.redhat.com
#
# script to take snapshot of yum repositories for given date.
#
# Jaroslaw.Polok@cern.ch
#

FORCE=1
DATE=`/bin/date +%Y%m%d`
TODO="rhel7 rhev7 rhrt7 rhel7-fastrack rhel8 rhel9"
SPREF="/mnt/data1/dist"
DPREF=$YUMSNAPSHOT_DESTINATION

for COMP in ${TODO}; do

  case "$COMP" in

    "rhel7")
      TOPSRC="cdn.redhat.com/content/dist/rhel/server/7/7Server"
      TOPDST=$TOPSRC
      PROT=""
      TOPPROT=""
      ARCHES="x86_64"
      REPOS="
        os
        debug
        optional/os
        optional/source/SRPMS
        optional/debug
        extras/os
        extras/source/SRPMS
        extras/debug
        supplementary/os
        supplementary/debug
        supplementary/source/SRPMS
        rhn-tools/source/SRPMS
        rhn-tools/os
        rhn-tools/debug
        oracle-java/os
        oracle-java/source/SRPMS
        rh-common/os
        rh-common/source/SRPMS
        rh-common/debug
        debug
        source/SRPMS
        rhev-mgmt-agent/3/debug
        rhev-mgmt-agent/3/os
        rhev-mgmt-agent/3/source/SRPMS
        rhv-mgmt-agent/4/debug
        rhv-mgmt-agent/4/os
        rhv-mgmt-agent/4/source/SRPMS
        v2win/os
        v2win/debug
        v2win/source/SRPMS
      "
    ;;

   "rhev7")
      TOPSRC="cdn.redhat.com/content/dist/rhel/server/7/7Server"
      TOPDST=$TOPSRC
      PROT=".rhev"
      TOPPROT="rhevh"
      ARCHES="x86_64"
      REPOS="
        rhevh/debug
        rhevh/os
        rhevh/source/SRPMS
        rhevm/3.5/os
        rhevm/3.5/source/SRPMS
        rhevm/3.5/debug
        rhevm/3.6/os
        rhevm/3.6/source/SRPMS
        rhevm/3.6/debug
        rhv/4.0/os
        rhv/4.0/debug
        rhv/4.0/source/SRPMS
        rhvh-build/4/os
        rhvh-build/4/debug
        rhvh-build/4/source/SRPMS
      "
    ;;

    "rhrt7")
      TOPSRC="cdn.redhat.com/content/dist/rhel/server/7/7Server"
      TOPDST=$TOPSRC
      PROT=".rhrt"
      TOPPROT="rt"
      ARCHES="x86_64"
      REPOS="
        rt/os
        rt/debug
        rt/source/SRPMS
      "
    ;;


    "rhel7-fastrack")
      TOPSRC="cdn.redhat.com/content/fastrack/rhel/server/7"
      TOPDST=$TOPSRC
      PROT=""
      TOPPROT=""
      ARCHES="x86_64"
      REPOS="
        debug
        os
        source/SRPMS
        optional/debug
        optional/os
        optional/source/SRPMS
      "
    ;;

    "rhel8")
      TOPSRC="cdn.redhat.com/content/dist/rhel8/8"
      TOPDST=$TOPSRC
      PROT=""
      TOPPROT=""
      ARCHES="x86_64"
      REPOS="
        appstream/os
        appstream/debug
        baseos/os
        baseos/debug
        baseos/source
        codeready-builder/os
        codeready-builder/debug
        codeready-builder/source
        rt/os
        rt/debug
        rt/source
        supplementary/os
        supplementary/debug
        supplementary/source
      "
    ;;

    "rhel9")
      TOPSRC="cdn.redhat.com/content/dist/rhel9/9"
      TOPDST=$TOPSRC
      PROT=""
      TOPPROT=""
      ARCHES="x86_64"
      REPOS="
        appstream/os
        appstream/debug
        appstream/source
        baseos/os
        baseos/debug
        baseos/source
        codeready-builder/os
        codeready-builder/debug
        codeready-builder/source
        rt/os
        rt/debug
        rt/source
        supplementary/os
        supplementary/debug
        supplementary/source
      "
    ;;

    *) echo "Error: unknown comp to snapshot"; exit 1 ;;
  esac

  echo "Starting `basename $0` at `date` for $COMP"

  if [ ! -d $DPREF/$DATE/cdn.redhat.com ]; then
    echo /bin/mkdir -p $DPREF/$DATE/cdn.redhat.com
    /bin/mkdir -p $DPREF/$DATE/cdn.redhat.com
  fi
  if [ ! -r $DPREF/$DATE/cdn.redhat.com/.htaccess ]; then
    echo /bin/ln -sf $DPREF/.rhel/.htaccess $DPREF/$DATE/cdn.redhat.com/.htaccess
    /bin/ln -sf $DPREF/.rhel/.htaccess $DPREF/$DATE/cdn.redhat.com/.htaccess
  fi

  for ARCH in ${ARCHES}; do
    for REPO in ${REPOS}; do
      if [ -d $DPREF/$DATE/$TOPDST/$ARCH/$REPO ] && [ $FORCE -ne 1 ]; then
        echo "skipping $COMP ($REPO), already there"
        continue
      fi

      [ $FORCE -eq 1 ] && echo /bin/rm -rf $DPREF/$DATE/$TOPDST/$ARCH/$REPO
      [ $FORCE -eq 1 ] && /bin/rm -rf $DPREF/$DATE/$TOPDST/$ARCH/$REPO
      echo /bin/mkdir -p $DPREF/$DATE/$TOPDST/$ARCH/$REPO
      /bin/mkdir -p $DPREF/$DATE/$TOPDST/$ARCH/$REPO
      echo "/bin/cp -Rl $SPREF/$TOPSRC/$ARCH/$REPO/* $DPREF/$DATE/$TOPDST/$ARCH/$REPO/"
      /bin/cp -Rl $SPREF/$TOPSRC/$ARCH/$REPO/* $DPREF/$DATE/$TOPDST/$ARCH/$REPO/

      echo /bin/rm -rf $DPREF/$DATE/$TOPDST/$ARCH/$REPO/{repodata,comps.xml,*-comps.xml.gz,updateinfo.xml,*-updateinfo.xml.gz,productid.gz}
      /bin/rm -rf $DPREF/$DATE/$TOPDST/$ARCH/$REPO/{repodata,comps.xml,*-comps.xml.gz,updateinfo.xml,*-updateinfo.xml.gz,productid.gz}
      echo /bin/mkdir -p $DPREF/$DATE/$TOPDST/$ARCH/$REPO/repodata
      /bin/mkdir -p $DPREF/$DATE/$TOPDST/$ARCH/$REPO/repodata
      echo "/usr/bin/rsync -rlptD $SPREF/$TOP/$TOPSRC/$ARCH/$REPO/repodata/ $DPREF/$DATE/$TOPDST/$ARCH/$REPO/repodata/"
      /usr/bin/rsync -rlptD $SPREF/$TOPSRC/$ARCH/$REPO/repodata/ $DPREF/$DATE/$TOPDST/$ARCH/$REPO/repodata/

      [ -f $SPREF/$TOPSRC/$ARCH/$REPO/comps.xml ] && /bin/cp $SPREF/$TOPSRC/$ARCH/$REPO/comps.xml $DPREF/$DATE/$TOPDST/$ARCH/$REPO/
      [ -f $SPREF/$TOPSRC/$ARCH/$REPO/updateinfo.xml ] && /bin/cp $SPREF/$TOPSRC/$ARCH/$REPO/updateinfo.xml $DPREF/$DATE/$TOPDST/$ARCH/$REPO/
      [ -f $SPREF/$TOPSRC/$ARCH/$REPO/productid.gz ] && /bin/cp $SPREF/$TOPSRC/$ARCH/$REPO/productid.gz $DPREF/$DATE/$TOPDST/$ARCH/$REPO/

      if [ "x$PROT" != "x" ] && [ "x$TOPPROT" != "x" ]; then
        echo /bin/ln -sf $DPREF/$PROT/.htaccess $DPREF/$DATE/$TOPDST/$ARCH/$TOPPROT/.htaccess
        /bin/ln -sf $DPREF/$PROT/.htaccess $DPREF/$DATE/$TOPDST/$ARCH/$TOPPROT/.htaccess
      fi
    done
  done

done

echo "Finished `basename $0` at `date`."
